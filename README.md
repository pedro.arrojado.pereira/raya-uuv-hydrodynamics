# Raya UUV Hydrodynamics

The Raya UUV Hydrodynamics repository contains files to obtain the hydrodynamic coefficients of the Added Mass matrix and the Damping matrix.

If you are using this repository for your publication, please consider citing our work, where you will find a more detailed approach to our methodology:
```
@inproceedings{Manhaes_2024,
	doi = {xx.xxxx/oceans.2024.xxxxxxx},
	url = {https://doi.org/xx.xxxx/oceans.2024.xxxxxxx},
	year = 2024,
	month = {sep},
	publisher = {{IEEE}},
	author = {João Leitão and Pedro Pereira and Raul Campilho and Andry Pinto},
	title = {Estimation of the Raya {UUV} Hydrodynamic Coefficients using OpenFOAM},
	booktitle = {{OCEANS} 2024 {MTS}/{IEEE} Hallifax}
}
```

## Dependencies

You will need a working installation of OpenFoam 11 in order to use these files.

## Ubuntu

- Ubuntu:
    - 20.04 LTS, codename focal
    - 22.04 LTS, codename jammy
    - 23.04, codename lunar (until May 2024)
    - 24.04 LTS, codename noble (from June 2024)
- [OpenFoam 11](https://openfoam.org/download/11-ubuntu/)
    

### Clone the workspace

```bash
git clone https://gitlab.com/pedro.arrojado.pereira/raya-uuv-hydrodynamics.git
```

# Usage

## Simulation Parameters

Depending on your application, you might need to readapt the simulation parameters.

### Linear Velocity Fields
Linear velocity fields can be altered in the "U" files. Below is an example of how to modify these fields:

```
inlet
    {
        type            uniformFixedValue;
        uniformValue    table
        (
          (0 (0 0 -0.2))    //(time_stamp (vx vy vz))            
          (0.2 (0 0 -0.2))  //(time_stamp (vx vy vz))              
          (0.3 (0 0 -0.4))  //(time_stamp (vx vy vz))   
        );
    }
```

### Angular Velocity Fields
Angular velocity fields can be altered in the "dynamicMeshDict" files. Below is an example of how to modify these fields:

```
omega
    {
        type            scale;
        scale
        {
            type            table;
            values
            (
                (0               1) 	// (time_stamp scale_factor)
                (0.095            1)	// (time_stamp scale_factor)
                (0.1            1.005)	// (time_stamp scale_factor)
            );
        }
        value           10; // Initial value of omega
    }
```
### Change Mesh size
To alter the domain size edit the "blockMeshDict" file. OpenFoam has a nice tutorial for [this](https://www.openfoam.com/documentation/user-guide/4-mesh-generation-and-conversion/4.3-mesh-generation-with-the-blockmesh-utility). The refined mesh is mainly defined by the "snappyHexMeshDict" file. Once again I refer to OpenFoam's documentation for [this](https://www.openfoam.com/documentation/user-guide/4-mesh-generation-and-conversion/4.4-mesh-generation-with-the-snappyhexmesh-utility).

### Replace the AUV.stl File

<p>For linear simulations, you can replace the AUV.stl file located in the "constant/triSurface" folder with your custom STL file. 
It is highly advisable to maintain the same file name (AUV.stl) to ensure compatibility.</p>

<p>For rotational simulations, the AUV.stl file is located in the "constant/geometry" folder. Additionally, you must replace the
 AUV-innerCylinder.stl file with a cylinder that encompasses the entire body of your custom object.</p>

## Linear simulations

For each simulation, a mesh must be generated. For example, to generate the mesh of the Added Mass linear surge acceleration do:

1. **Navigate to the simulation directory:**
```bash
cd raya-uuv-hydrodynamics/added_mass/linear/surge/
```

2. **Generate initial domain Mesh with BlockMesh**
```bash
blockMesh
surfaceFeatures
```

### Single-Processor Execution

3. **Generate refined Mesh with SnappyHexMesh**
```bash
snappyHexMesh
```

4. **Run the simulation**
```bash
pisoFoam
``` 

### Parallel Execution

3. **Generate refined Mesh with SnappyHexMesh**
```bash
decomposePar
mpirun --oversubscribe -np 8 renumberMesh -overwrite -parallel
mpirun --oversubscribe -np 8 snappyHexMesh -overwrite -parallel
reconstructPar
```

4. **Run the simulation**
```bash
decomposePar
mpirun --oversubscribe -np 8 renumberMesh -overwrite -parallel
mpirun --oversubscribe -np 8 pisoFoam -parallel
reconstructPar
``` 


## Rotational simulations
For each simulation, a mesh must be generated. For example, to generate the mesh of the Added Mass rotational roll acceleration do:

1. **Navigate to the simulation directory:**
```bash
cd raya-uuv-hydrodynamics/added_mass/rotational/roll/
```

2. **Generate initial domain Mesh with BlockMesh**
```bash
blockMesh
surfaceFeatures
```

### Single-Processor Execution

3. **Generate refined Mesh with SnappyHexMesh**
```bash
snappyHexMesh
createBaffles -overwrite
splitBaffles -overwrite
renumberMesh -noFields -overwrite
createNonConformalCouples -overwrite nonCouple1 nonCouple2
```

4. **Run the simulation**
```bash
foamRun
```

### Parallel Execution

3. **Generate refined Mesh with SnappyHexMesh**
```bash
decomposePar
mpirun --oversubscribe -np 8 renumberMesh -overwrite -parallel
mpirun --oversubscribe -np 8 snappyHexMesh -overwrite -parallel
reconstructPar
createBaffles -overwrite
splitBaffles -overwrite
renumberMesh -noFields -overwrite
createNonConformalCouples -overwrite nonCouple1 nonCouple2
```
4. **Run the simulation**
```bash
decomposePar
mpirun --oversubscribe -np 8 foamRun -parallel
reconstructPar
```


# License

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](LICENSE) for more details.

This project makes use of other open source software, for full details see the
file [LICENSE_THIRDPARTY](LICENSE_THIRDPARTY).
